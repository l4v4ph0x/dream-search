var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

class Search {

  parse (data, deepData, searchX, enableRegexSearch = false) {
    var self = this;

    var ret = [];

    var maxDescriptionLength = 300;
    var startTime = Date.now();
    //var sdata = data.toLowerCase();
    var ssearchX = escapeHtml(searchX.toLowerCase());
    if (enableRegexSearch == false) {
      ssearchX = escapeRegExp(ssearchX);
    }

    // search words gets compressed for fstaer search with deepsearch only rn
    var deepSearchX = ssearchX;
    data += deepData


    compresses.forEach(function(c) {
      var csplits = c.split("=");
      deepSearchX = deepSearchX.replace(new RegExp(escapeRegExp(csplits[1]), "g"), String.fromCharCode(parseInt(csplits[0])));
    });

    // make searched
    var searches = [];
    var searchbuildstrnoneedafter = "";
    var searchsplit = ssearchX.split(" ");

    for (var i = 1; i < searchsplit.length; i++) {
      searches.push(searchsplit[i]);
    }

    for (var i = 0; i < searchsplit.length; i++) {
      searchbuildstrnoneedafter += (i == 0 ? "" : " ") + searchsplit[i];
      searches.push(searchbuildstrnoneedafter);
    }

    var regexo = new RegExp("(( |^)"+searches.join("[ |$]|( |^)")+"[ |$])", "gi");


    console.log(searches);


    // add deep lines to data except first that holds compression data

    /*
     *  score got way too complicated
     *
     *
     */


    for (var i = 0; i < data.length; i++) {
      var index = data.indexOf("\n", i);

      var searchXX = ssearchX;
      var splitted = data.substring(i, index).split("\t");
      var hashUrl = splitted[0];
      var bitUrl = "";
      var deep = "";
      var title = escapeHtml(splitted[1]);
      var description = escapeHtml(splitted[2]);
      var score = 0;

      var descriptionFirstIndex = 0;

      // fill rest of description
      for (var j = 3; j < splitted.length; j++) description += splitted[j] + " ";

      if (hashUrl.charAt(0) == '-') {
        searchXX = deepSearchX;
        var deepsplit = hashUrl.split("/");
        deep = escapeHtml(hashUrl.substr(deepsplit[0].length));
        hashUrl = deepsplit[0].substr(1);
      }

      if (hashUrl.charAt(0) == '+') {
        var urlsplit = hashUrl.substr(1).split("|");
        hashUrl = urlsplit[0];
        bitUrl = escapeHtml(urlsplit[1]);
      }

      hashUrl = escapeHtml(hashUrl);
      var shashUrl = hashUrl.toLowerCase();
      var sbitUrl = bitUrl.toLowerCase();
      var stitle = title.toLowerCase();
      var sdescription = description.toLowerCase();

      searches.forEach(function(s) {
        if (s.length > 3) {
          var oneIndex = shashUrl.indexOf(s);
          if (oneIndex != -1) {
            if (shashUrl.length == oneIndex + s.length || shashUrl[oneIndex + s.length] == " ") {
              if (oneIndex == 0 || shashUrl[oneIndex -1] == " ") {
                score += 100 * (s.length / 2);
              }
            }
          }

          oneIndex = sbitUrl.indexOf(s);
          if (oneIndex != -1) {
            if (sbitUrl.length == oneIndex + s.length || sbitUrl[oneIndex + s.length] == " ") {
              if (oneIndex == 0 || sbitUrl[oneIndex -1] == " ") {
                score += 90 * (s.length / 2);
              }
            }
          }

          oneIndex = stitle.indexOf(s);
          if (oneIndex != -1) {
            if (stitle.length == oneIndex + s.length || stitle[oneIndex + s.length] == " ") {
              if (oneIndex == 0 || stitle[oneIndex -1] == " ") {
                score += 70 * (s.length / 2);
              }
            }
          }

          oneIndex = sdescription.indexOf(s);
          if (oneIndex != -1) {
            if (sdescription.length == oneIndex + s.length || sdescription[oneIndex + s.length] == " ") {
              if (oneIndex == 0 || sdescription[oneIndex -1] == " ") {
                score += 55 * (s.length / 2);

                // overwrite it over because always last search is the best
                descriptionFirstIndex = oneIndex;
              }
            }
          }
        }
      });

      if (score > 0) {
        //title += " " + score;

        if (description.length > maxDescriptionLength) {
          description = description.substr(descriptionFirstIndex, maxDescriptionLength);
        }

        ret.push({"score": score, "hashUrl": hashUrl, "bitUrl": bitUrl, "deep": deep, "title": title, "description": description});
      }

      i = index;
    }

    console.log("search took time: in ms " + (Date.now() - startTime));
    console.log("search got results: " + ret.length);

    return ret;
  }
}

search = new Search();
