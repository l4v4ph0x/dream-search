// add key down function
var shorcuts = `
<div id="autocomplete" class="search__autocomplete search__autocomplete--bang" style="position: absolute; display: block;">
  <div class="acp-wrap js-acp-wrap">
    <div class="acp-wrap__column acp-wrap__column--left">
      <div class="acp acp--bang" data-index="0">
        <!--div class="acp--bang__img-wrap"></div-->
        <div class="acp--bang__body"><span class="acp--bang__phrase">!wp</span><span class="acp--bang__snippet">ZeroWiki <- page on </span></div>
      </div>
    </div>
    <div class="acp-wrap__column acp-wrap__column--right">
      <!--div class="acp acp--bang" data-index="0">
          <div class="acp--bang__img-wrap"></div>
          <div class="acp--bang__body"><span class="acp--bang__phrase">!w</span><span class="acp--bang__snippet">ZeroWiki</span></div>
      </div-->
    </div>
  </div>
</div>
`;

var searchoxElem = document.querySelector("#search_form_input");

if (searchoxElem) {
  searchoxElem.addEventListener('keyup', function () {

    if (searchoxElem.value.indexOf(" ") != -1 || !searchoxElem.value.startsWith("!")) {
      var autocompleteEelm = document.querySelector("#autocomplete");

      if (autocompleteEelm) {
        autocompleteEelm.remove();
      }

    } else if (searchoxElem.value.startsWith("!")) {
      var autocompleteEelm = document.querySelector("#autocomplete");

      if (!autocompleteEelm) {
        var autosugestionPlaceElem = document.querySelector("#autosugestion-place");
        if (autosugestionPlaceElem) {
          autosugestionPlaceElem.innerHTML = shorcuts;
        }
      }
    }
  });
}
