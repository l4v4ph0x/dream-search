function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&|#]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

Array.prototype.extend = function (other_array) {
    /* You should include a test to check whether other_array really is an array */
    other_array.forEach(function(v) {this.push(v)}, this);
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

var printedOutCount = 0;
var printSize = 10;
var searchRet = []
// deep word compresses and keep saerch speed fast
var compresses = [];

class ZeroDream extends ZeroFrame {
  addResult (reload = false) {
    var self = this;
    var elem = document.querySelector("#links");

    if (elem) {
      if (reload || printedOutCount == 0) {
        // clear all link to load them again
        elem.innerHTML = "";
      }

      searchRet.sort(function(a, b) {
        if (a["score"] < b["score"]) return 1;
        if (a["score"] > b["score"]) return -1;
        return 0;
      });

      if (searchRet.length == 0) {
        var searchX = getURLParameter("q");

        elem.innerHTML += `
        <p class="no-results__title">No results found for <b>`+searchX+`</b>.</p>
        <div class="no-results__tips"><p class="no-results__tips-title">Suggestions:</p>
          <ul class="no-results__tips-list">
            <li>Make sure all words are spelled correctly.</li>
            <li>Try different keywords.</li>
            <li>Try more general keywords.</li>
            <li>Try fewer keywords.</li>
          </ul>
        </div>
        `;

        // leave function as theres no need to do nothing anymore here
        return;
      }

      self.getSettings(function(data) {

        var searchX = getURLParameter("q");
        var ssearchX = escapeRegExp(escapeHtml(searchX.toLowerCase()));
        var highlight_repl = new RegExp("("+ssearchX.split(" ").join("\\b|\\b")+")", "gi");
        console.log("("+ssearchX.split(" ").join("\\b|\\b")+")");

        for (var i = (reload ? 0 : printedOutCount); i < searchRet.length; i++) {
          // fix compression if deep is searched
          if (searchRet[i]["deep"] != "") {
            // and so <span class='highlight'>
            compresses.forEach(function(c) {
              var csplits = c.split("=");

              var repl = new RegExp(escapeRegExp(String.fromCharCode(parseInt(csplits[0]))), "g");

              searchRet[i]["hashUrl"] = searchRet[i]["hashUrl"].replace(repl, csplits[1]);
              searchRet[i]["bitUrl"] = searchRet[i]["bitUrl"].replace(repl, csplits[1]);
              searchRet[i]["deep"] = searchRet[i]["deep"].replace(repl, csplits[1]);
              searchRet[i]["title"] = searchRet[i]["title"].replace(repl, csplits[1]);
              searchRet[i]["description"] = searchRet[i]["description"].replace(repl, csplits[1]);
            });
          }

          // set highlights
          searchRet[i]["hashUrl"] = searchRet[i]["hashUrl"].replace(highlight_repl, "<span class='highlight'>$1</span>")
          searchRet[i]["deep"] = searchRet[i]["deep"].replace(highlight_repl, "<span class='highlight'>$1</span>")
          searchRet[i]["title"] = searchRet[i]["title"].replace(highlight_repl, "<span class='highlight'>$1</span>")
          searchRet[i]["description"] = searchRet[i]["description"].replace(highlight_repl, "<span class='highlight'>$1</span>")


          var visitUrl = (data["open link"] == undefined || data["open link"] == 0) && searchRet[i]["bitUrl"] != "" ? searchRet[i]["bitUrl"] : searchRet[i]["hashUrl"];
          visitUrl = visitUrl.replace(new RegExp("<[^>]*>", "g"), "") + searchRet[i]["deep"];

          var extra = "";

          if (searchRet[i]["bitUrl"] != "") {
            extra = `
              <img data-src="assets/namecoin.png" height="16" width="16" title="`+searchRet[i]["bitUrl"]+`" class="result__icon__img js-lazyload-icons" src="assets/namecoin.png">
            `;
          }

          elem.innerHTML += `
          <div class="result results_links_deep highlight_d result--url-above-snippet">
            <div class="result__body links_main links_deep">
              <h2 class="result__title">
                <a class="result__a" rel="noopener" href="http://`+window.location.hostname+`:`+window.location.port+`/`+visitUrl+`">
                  `+extra+`
                  `+searchRet[i]["title"]+`
                </a>
              </h2>
              <div class="result__extras js-result-extras">
                <div class="result__extras__url">
                  <a href="http://`+window.location.hostname+`:`+window.location.port+`/`+visitUrl+`" rel="noopener" class="result__url js-result-extras-url">
                    <span class="result__url__domain">`+searchRet[i]["hashUrl"]+`</span><span class="result__url__full">`+searchRet[i]["deep"]+`</span>
                  </a>
                </div>
              </div>
              <div class="result__snippet js-result-snippet">`+searchRet[i]["description"]+`</div>
            </div>
          </div>
          `;


          if (i >= printedOutCount + (reload ? 0 : printSize)) {
            elem.innerHTML += `
            <div class="result result--more" id="rld-1"><a onclick="javascript:page.search();" class="result--more__btn btn btn--full">More results</a></div>
            `;

            printedOutCount = i +1;
            break;
          }

          if (i == searchRet.length -1) {
            printedOutCount = i +1;
          }
        }
      });
    }
  }

  loadSearchBar () {
    var self = this;
    var searchX = getURLParameter("q");
    var elem = document.querySelector("#search_form_input");

    if (elem) {
      elem.value = searchX;
    }
  }

  parseSearch (binHash) {

    var self = this;
    var searchX = getURLParameter("q");
    if (searchX == null) searchX = "";

    // check answers
    var elem = document.querySelector("#answer");
    if (elem) {
      var res = new colorCodes().parseSearch(searchX);
      if (res != "") {
        elem.innerHTML = res;
      } else {
        res = new hash().parseSearch(searchX);
        if (res != "") {
          elem.innerHTML = res;
        }
      }
    }


    self.cmd("fileGet", ["crawled-"+binHash+".bin"], (data) => {
      if (data) {
        /*
        self.cmd("fileGet", ["deepcrawled-"+binHash+".bin"], (deepData) => {
          if (deepData) {
            compresses = deepData.substr(0, deepData.indexOf("\n")).split("\t");


          } else {
            console.error("ERROR - deep data is null");
          }
        });
        */

        var startTime = Date.now();

        var fullDeepData = "";
        var filesLoaded = 0;
        var maxFiles = 0;
        var loadingDeepElem = document.querySelector("#loading_deep");
        var statusDeepElem = document.querySelector("#status_deep");
        var maxDeepElem = document.querySelector("#max_deep");

        var files = [
          "static/deepcrawls/deepcrawled-0.txt",
          "static/deepcrawls/deepcrawled-1.txt",
          "static/deepcrawls/deepcrawled-2.txt",
          "static/deepcrawls/deepcrawled-3.txt",
          "static/deepcrawls/deepcrawled-4.txt",
          "static/deepcrawls/deepcrawled-5.txt",
          "static/deepcrawls/deepcrawled-6.txt",
          "static/deepcrawls/deepcrawled-7.txt",
          "static/deepcrawls/deepcrawled-8.txt",
          "static/deepcrawls/deepcrawled-9.txt"
        ];
        console.log(files);

        files.forEach(function(file) {
          maxFiles++;

          self.cmd("fileGet", [file], (deepData) => {
            if (deepData && deepData.charAt(0) != deepData.charAt(10000)) {

              //compresses += deepData.substr(0, deepData.indexOf("\n")).split("\t");
              //fullDeepData += deepData;

              console.log(file+ " took: in ms " + (Date.now() - startTime));
              searchRet = searchRet.concat(search.parse("", deepData, searchX));
              filesLoaded++;
              if (statusDeepElem) {
                statusDeepElem.innerHTML = filesLoaded + "";
              }

            } else {
              console.error("ERROR - deep data is null: " + file);
            }

            if (filesLoaded == maxFiles) {
              console.log("deep files loading: in ms " + (Date.now() - startTime));
              console.log("deep results:  " + searchRet.length);

              if (loadingDeepElem) {
                loadingDeepElem.innerHTML = "Loading deep: done";
              }

              // add results with realod, so were gonna load as much results as we were lookin
              self.addResult(true);
            }
          });

        });
        maxDeepElem.innerHTML = maxFiles + "";


        searchRet = search.parse(data, "", searchX);
        self.addResult();
      } else {
        console.error("ERROR - data is null");
      }
    });
  }

  search (reload = false) {
    console.log("lilFrame.js - search()");

    var self = this;

    if (reload) {
      self.addResult(reload);
      return;
    }


    if (printedOutCount != 0) {
      var elem = document.querySelector("#rld-1");
      if (elem) {
        elem.remove()
      }
      self.addResult();
      return;
    }

    self.parseSearch("8");
  }

  getSettings (callback) {
    var self = this;

    self.cmd("userGetSettings", [], (res) => {
      callback(res);
    });
  }

  loadOpenLink () {
    var self = this;
    var elemA = document.querySelector("#dropdown-openlink a");
    var modalElem = document.querySelector("#modal-dropdown-openlink");
    var modalElemAs = document.querySelectorAll("#modal-dropdown-openlink a");

    modalElemAs.forEach(function(a) {
      a.className = a.className.replace(" is-selected", "");
    });

    // load open link by from user settings
    self.getSettings(function(data) {
      if (elemA) {
        if (data["open link"] == undefined || data["open link"] == 0) {
          elemA.innerHTML = "Open link: bit if possible";
          modalElemAs[0].className += " is-selected";
        } else {
          elemA.innerHTML = "Open link: Only Hash"
          modalElemAs[1].className += " is-selected";
        }
      }
    });

    if (modalElem.className.indexOf(" is-showing") != -1) {
      modalElem.className = modalElem.className.replace(" is-showing", "");
    }
  }

  setSettings_openLink (index, e) {
    console.log("lilFrame.js - setSettings_openLink(index)");

    var self = this;

    self.getSettings(function(data) {
      data["open link"] = index;

      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - setSettings_openLink: userSetSettings " + res);
        self.loadOpenLink();
        // just realod, that the boolean true here
        self.search(true);
      });
    });
  }
}

page = new ZeroDream();
