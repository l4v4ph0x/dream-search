
class colorCodes {
  // ext function will be at ext as js
  toHSL (hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    var r = parseInt(result[1], 16);
    var g = parseInt(result[2], 16);
    var b = parseInt(result[3], 16);

    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min){
        h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

      switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
      }

      h /= 6;
    }

    s = s*100;
    s = Math.round(s);
    l = l*100;
    l = Math.round(l);
    h = Math.round(360*h);

    return h+", "+s+"%, "+l+"%";
  }

  // good source https://gist.github.com/felipesabino/5066336
  RGBtoCMYK (r, g, b) {
    r = r / 255;
    g = g / 255;
    b = b / 255;

    var k = Math.min( 1 - r, 1 - g, 1 - b );
    var c = ( 1 - r - k ) / ( 1 - k );
    var m = ( 1 - g - k ) / ( 1 - k );
    var y = ( 1 - b - k ) / ( 1 - k );

    return Math.round(c * 100)+"%, "+Math.round(m * 100)+"%, "+Math.round(y * 100)+"%, "+Math.round(k * 100);
  }

  parseSearch (searchX) {
    var ret = "";

    var self = this;

    // load up color answer
    if (searchX.startsWith("#") && (searchX.length == 4 || searchX.length == 7)) {
      var color = searchX.substr(1);
      var colorDec = "";
      var colorHsl = "";
      var colorCmyk = "";

      if (color.length == 3) {
        color = color.charAt(0)+color.charAt(0)+color.charAt(1)+color.charAt(1)+color.charAt(2)+color.charAt(2);
      }

      color = color.toUpperCase();
      colorDec = parseInt(color.charAt(0)+color.charAt(1), 16)+", "+parseInt(color.charAt(2)+color.charAt(3), 16)+", "+parseInt(color.charAt(4)+color.charAt(5), 16);
      colorHsl = self.toHSL(color);
      colorCmyk = self.RGBtoCMYK(parseInt(color.charAt(0)+color.charAt(1), 16), parseInt(color.charAt(2)+color.charAt(3), 16), parseInt(color.charAt(4)+color.charAt(5), 16));

      ret += `
      <div id="zero_click_wrapper" class="zci-wrap"><div class="zci  zci--color_codes is-active" id="zci-color_codes"><div class="cw ">
        <div class="zci__main  c-base">
          <div class="zci__body">
            <div class="c-base__content ">
              <span class="colorcodesbox circle" style="background: #`+color+`"></span>
              <div class="column1 tx-clr--dk2">
                  <p class="hex tx-clr--dk zci__caption">Hex: #`+color+`</p>
                  <p class="no_vspace">RGBA(`+colorDec+`, 1)</p>
                  <p class="no_vspace">HSL(`+colorHsl+`)</p>
                  <p class="no_vspace">CMYK(`+colorCmyk+`)</p>
              </div>
            </div>
            <div class="c-base__links">
            </div>
          </div>
        </div>
      </div>
      `;
    }

    return ret;
  }
}
