
class hash {
  parseSearch (searchX) {
    var ret = "";

    var self = this;

    // load up color answer
    if (searchX.startsWith("base64 ")) {
      var toHash = searchX.substr(7);

      ret += `
      <div id="zero_click_wrapper" class="zci-wrap">
        <div class="zci  zci--base64 is-active" id="zci-base64">
          <div class="cw "><div class="zci__main  c-base">
            <div class="zci__body"><h3 class="c-base__title">`+btoa(toHash)+`</h3>
            <h4 class="c-base__sub">Base64 encode: `+toHash+`</h4>
            <div class="c-base__links"></div>
          </div>
        </div>
      </div>
      `;
    }

    return ret;
  }
}
