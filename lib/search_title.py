import selenium
from selenium import webdriver

from html.parser import HTMLParser
from lib.MLStripper import MLStripper

class search_title:
    def __init__(self, driver):
        self.driver = driver

        self.tags_that_might_contain_title = [
            "h1"
        ]

    def try_your_best(self):
        """
        returns: None driver problem, object not acceptable
        """

        titles = []

        try:
            # get main title given by zeronet content.json
            title_elem = self.driver.find_element_by_tag_name("title")
            title = self.get_elem_text(title_elem)
            if title != "":
                titles.append(title.replace("\n", ""))

            # switch to iframe since zeronet create website in iframe
            self.driver.switch_to.frame(self.driver.find_element_by_tag_name("iframe"))

            # search more title by something idk
            for might_be_title in self.tags_that_might_contain_title:
                try:
                    title_elem = self.driver.find_element_by_tag_name(might_be_title)
                    title = self.get_elem_text(title_elem)
                    if title != "":
                        titles.append(title.replace("\n", ""))
                except selenium.common.exceptions.NoSuchElementException:
                    pass
        except Exception:
            return None

        try:
            # let the best bet to be at the bottom
            # get main title set by content owner
            title_elem = self.driver.find_element_by_tag_name("title")
            title = self.get_elem_text(title_elem)
            if title != "":
                titles.append(title.replace("\n", ""))
        except Exception:
            pass

        try:
            # switch back driver
            self.driver.switch_to.default_content()
        except Exception:
            pass


        return titles

    def get_elem_text(self, elem):
        """
        get safely element innerHTML
        """

        ret = ""

        try:
            if elem != None:
                text = elem.get_attribute('innerHTML')
                if text != None and str(text) != "":
                    ret = text
        except Exception:
            pass

        return ret
