import selenium
from selenium import webdriver

import html
from html.parser import HTMLParser
from lib.MLStripper import MLStripper

import json

class search_description:
    def __init__(self, driver = None, etc_driver = None):
        self.driver = driver
        self.etc_driver = etc_driver

        self.tags_that_might_contain_description = [
            "h1[@class='topics-title']",
            "h2[@data-editable='description']",
            "h2",
            "div[@class='body']"
        ]

        self.no_need_strings_starts_with = [
            "# H1 ## H2 ### H3 italic **bold**"
        ]

    def try_your_best(self):
        """
        returns: None driver problem, object not acceptable
        """

        descriptions = []

        try:
            # try get description from content json
            self.etc_driver.get(self.driver.current_url + "/content.json")
            html_elem = self.etc_driver.find_element_by_tag_name("html")
            jo = json.loads(self.get_elem_text(html_elem))

            if "description" in jo.keys():
                descriptions.append(jo["description"].replace("\n", ""))

            # switch to iframe since zeronet create website in iframe
            self.driver.switch_to.frame(self.driver.find_element_by_tag_name("iframe"))

            try:
                # overwrite because, this is best bet
                description_elem = self.driver.find_element_by_tag_name("meta[@name='description']")
                description = self.get_elem_text(description_elem, "content")
                if description != "":
                    descriptions = [description.replace("\n", "")]
            except selenium.common.exceptions.NoSuchElementException:
                pass

            for might_be_description in self.tags_that_might_contain_description:
                try:
                    description_elem = self.driver.find_element_by_tag_name(might_be_description)
                    description = self.get_elem_text(description_elem)
                    if description != "":
                        descriptions.append(description.replace("\n", ""))
                except selenium.common.exceptions.NoSuchElementException:
                    pass

            # add site first text
            html_elem = self.driver.find_element_by_tag_name("body")
            description = " ".join(self.remove_wierd(html.unescape(self.get_elem_text(html_elem))).split())
            if description != "":

                # check if description starts with onvalid description
                no_need_this = False
                for no_need in self.no_need_strings_starts_with:
                    if description.startswith(no_need):
                        no_need_this = True
                        break

                if not no_need_this:
                    descriptions.append(description[:200].replace("\n", ""))

            # switch back driver
            self.driver.switch_to.default_content()
        except Exception:
            return None

        return descriptions

    def get_elem_text(self, elem, attr = "innerHTML"):
        """
        get safely element innerHTML
        """

        ret = ""

        try:
            if elem != None:
                text = elem.get_attribute(attr)
                if text != None and str(text) != "":
                    s = MLStripper()
                    s.feed(text)
                    ret = s.get_data()
        except Exception:
            pass

        return ret

    def remove_wierd(self, str):
        return str.replace("\n", " ").replace("\r", "").replace("\\", "").replace("/", "").replace("<", "").replace(">", "").replace("_", "").replace(".", " ").replace("\"", "").replace("'", "").replace("(", "").replace(")", "").replace("|", "").replace(")", "\\u200b").replace("\t", "").replace("  ", " ").replace("  ", " ").replace("  ", " ")
