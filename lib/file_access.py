import html

class file_access:
    def __init__(self, debug):
        self.debug = debug


    def add_crawl_line(self, new_crawl_line, crawl_file = "crawl.bin"):
        if self.debug:
            print(f"file_access - add_crawl_line({id}, {new_crawl_line})")

        with open(crawl_file, "a", encoding="utf8") as f:
            f.write(new_crawl_line)

    def update_crawl_line(self, id, new_crawl_line, crawl_file = "crawl.bin"):
        if self.debug:
            print(f"file_access - update_crawl_line({id}, {new_crawl_line})")

        new_crawl_line = new_crawl_line.replace("\n", " ").replace("\r", " ")

        crawl_lines = ""
        found = False
        with open(crawl_file, "r", encoding="utf8") as f:
            # fuck windows and its \r
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                id_to_check = line.split("\t")[0] if line[0] != '+' else line.split("\t")[0].split("|")[1]
                if id != id_to_check:
                    crawl_lines += line.replace("\n", " ").replace("\r", " ") + "\n"
                else:
                    found = True
                    crawl_lines += new_crawl_line + "\n"

        if not found:
            crawl_lines += new_crawl_line + "\n"

        with open(crawl_file, "w", encoding="utf8") as f:
            f.write(crawl_lines)


    def remove_crawl_line(self, id):
        if self.debug:
            print(f"file_access - remove_crawl_line({id})")

        crawl_lines = ""
        with open("crawl.bin", "r", encoding="utf8") as f:
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                id_to_check = line.split("\t")[0] if line[0] != '+' else line.split("\t")[0].split("|")[1]
                if id != id_to_check:
                    crawl_lines += line + "\n"

        with open("crawl.bin", "w", encoding="utf8") as f:
            f.write(crawl_lines)

        with open("ignore.bin", "a", encoding="utf8") as f:
            f.write(f"{id}\n")


    def move_to_loading(self, id):
        if self.debug:
            print(f"file_access - move_to_loading({id})")

        crawl_lines = ""
        with open("crawl.bin", "r", encoding="utf8") as f:
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                id_to_check = line.split("\t")[0] if line[0] != '+' else line.split("\t")[0].split("|")[1]
                if id != id_to_check:
                    crawl_lines += line + "\n"

        with open("crawl.bin", "w", encoding="utf8") as f:
            f.write(crawl_lines)

        with open("loading.bin", "a", encoding="utf8") as f:
            f.write(f"{id}\n")


    def set_current_crawl_status(self, id):
        if self.debug:
            print("file_access - set_current_crawl_status({id})")

        with open("current.bin", "w", encoding="utf8") as f:
            f.write(f"{id}")

    def get_current(self):
        if self.debug:
            print(f"file_access - get_current()")

        with open("current.bin", "r", encoding="utf8") as f:
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                return line
        return ""

    def get_ignored(self):
        if self.debug:
            print(f"file_access - get_ignored()")

        ret = []
        with open("ignore.bin", "r", encoding="utf8") as f:
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                ret.append(line)
        return ret

    def get_crawl(self, ignore_sites, current_crawl_status, crawl_file = "crawl.bin"):
        if self.debug:
            print(f"file_access - get_crawl()")

        ret = []

        with open(crawl_file, "r", encoding="utf8") as f:
            readLinks = False
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue


                #if (current_crawl_status != "" and line.split("\t")[0] == current_crawl_status) or readLinks or current_crawl_status == "":
                #    readLinks = True
                #else:
                #    continue

                if line.split("\t")[0] not in ignore_sites:
                    #print(line.split("\t"))
                    title = line.split("\t")[1] if len(line.split("\t")) > 1 else "<loading title>"
                    ret.append({"id": html.unescape(line.split("\t")[0]).replace(" ", ""), "title": title, "line": line})

        return ret

    def get_loading_sites(self, ignore_sites):
        if self.debug:
            print(f"file_access - get_loading_sites()")

        ret = []

        with open("loading.bin", "r", encoding="utf8") as f:
            lines = f.read().split("\n")
            for line in lines:
                line = line.replace("\n", "")
                if line == "":
                    continue

                #print(line)
                ret.append({"id": line, "title": "<site loading>", "line": line})

        return ret
