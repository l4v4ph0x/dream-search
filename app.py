import selenium
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from os import listdir
from os.path import isfile, join

import sys
import time
import platform
import difflib
import json
import threading

from lib.MLStripper import MLStripper
from lib.search_title import search_title
from lib.search_description import search_description
from lib.file_access import file_access


already_got_links = []
already_got_deep = []

# sites that cuase uber lag and not worth look into, no more time wasting here
ignore_sites = ["1Name2NXVi1RDPDgf5617UoW7xA6YrhM9F", "dns.bit", "ZeroTorrent.bit", "1uPLoaDwKzP6MCGoVzw48r4pxawRBdmQc", "1CTo2UaBP9h7U5FzDEVzzc2mDPfCPSoSAK"]
#ignore_descriptions = ["0 Comments"]
# totally laggy sites, going to remove them from list also, useless sites cant use if javascript cause them to freeze
totally_ignore_url = ["1CTo2UaBP9h7U5FzDEVzzc2mDPfCPSoSAK", "1HeLLo4uzjaLetFx6NH3PMwFP3qbRbTf3D"]

links_to_click = ["show more", "More sites"]
links_depth = 40

current_crawl_status = ""


def get_crawl_line(id, _driver, _etc_driver):
    title_str = search_title(_driver).try_your_best()
    if title_str == None:
        print(f"{id} failed to get title")
        return None
    title_str = title_str[len(title_str) -1]

    description = search_description(_driver, _etc_driver).try_your_best()
    if description == None:
        print(f"{id} failed to get description")
        return None
    description = "\t".join(description)

    if id.endswith(".bit"):
        try:
            # get address from content.json
            _etc_driver.get(_driver.current_url + "/content.json")
            html_elem = _etc_driver.find_element_by_tag_name("html")
            jo = json.loads(search_description().get_elem_text(html_elem))

            if "address" in jo.keys():
                id = "+" + jo["address"] + "|" + id
        except:
            pass

    print(f"return {id}\t{title_str}\ndescription: {description}\n")
    return f"{id}\t{title_str}\t{description}\n"

def search_links(_driver, sleeptime):
    ###### searching links

    try:
        _driver.switch_to.frame(_driver.find_element_by_tag_name("iframe"))
    except selenium.common.exceptions.NoSuchElementException:
        _driver.switch_to.default_content()
        _driver.switch_to.frame(_driver.find_element_by_tag_name("iframe"))
    except Exception:
        return None

    links = []

    for i in range(0, links_depth):
        print(f"links_depth: {i} to {links_depth}")
        try:
            links = _driver.find_elements_by_tag_name("a")
        except:
            break
        print(f"links count: {len(links)}")

        # cant handle that many links
        if len(links) > 10000:
            break

        found = False

        for link in links:
            try:
                link_innerHTML = link.get_attribute("innerHTML")
            except Exception:
                continue

            for to_click in links_to_click:
                try:
                    if link_innerHTML.lower().startswith(to_click.lower()):
                        print(f"click: {link.get_attribute('href')}")

                        link.click()
                        found = True
                        time.sleep(0.1)
                        break
                except Exception:
                    continue

        if not found:
            break

    if len(links) == 0:
        print(f"{got_link['id']} has no links")
        return

    print(f"going to crawl site {got_link['id']} for links")

    link_ids = [l["id"] for l in already_got_links]

    for link in links:
        try:
            link_href = link.get_attribute("href")
        except Exception:
            continue

        if link_href is None:
            continue

        if link_href.startswith("http://127.0.0.1:43110/"):
            # click on link and let to download, it takes time anyway
            try:
                site_name = link_href[23:].split("/")[0] if "/" in link_href[23:] else link_href[23:]

                if site_name in link_ids or site_name in ignore_sites:
                    continue
            except Exception:
                print(f"error url: {link_href}")
                continue

            print(f"add loading site: {site_name}")
            already_got_links.append({"id": site_name, "title": "<load title>", "line": site_name})
            link_ids.append(site_name)
            file_access(debug=False).add_crawl_line(f"{site_name}\t<load title>\n", f"crawls/crawl-{sleeptime}.bin")

def search_deep(_driver, sleeptime):
    #### deep search

    # click on some links and crawling deeper
    try:
        _driver.switch_to.frame(_driver.find_element_by_tag_name("iframe"))
    except selenium.common.exceptions.NoSuchElementException:
        _driver.switch_to.default_content()
        _driver.switch_to.frame(_driver.find_element_by_tag_name("iframe"))
    except Exception:
        return None

    links = _driver.find_elements_by_tag_name("a")
    link_locs = []

    for link in links:
        try:
            link_loc = link.get_attribute("href")

            if (link_loc.startswith("http://127.0.0.1:43110/") and "/?" in link_loc) or link_loc.endswith(".html"):
                link_locs.append(link_loc)
        except:
            continue

    print(f"deep got {len(link_locs)} links to click on {got_link['id']}")
    for link_loc in link_locs:
        extended_link_loc = f"http://127.0.0.1:43110/{got_link['id']}/" if not (link_loc.startswith("/") or link_loc.startswith("http://127.0.0.1")) else ""

        while True:
            try:
                _driver.get(extended_link_loc + link_loc)
                break
            except selenium.common.exceptions.InvalidArgumentException:
                _driver.switch_to_default_content()
                continue
            except selenium.common.exceptions.TimeoutException:
                print(f"{extended_link_loc + link_loc} didn't load in time")
                break
        print(f"deep clicked on: {extended_link_loc + link_loc}")

        time.sleep(2)

        _driver.switch_to.frame(_driver.find_element_by_tag_name("iframe"))

        # only h3 h2 and h1 are accepted as to read something further
        try:
            main_text = _driver.find_element_by_tag_name("h2")
        except selenium.common.exceptions.NoSuchElementException:
            main_text = ""

        if main_text == None or main_text == "":
            try:
                main_text = _driver.find_element_by_tag_name("h1")
            except selenium.common.exceptions.NoSuchElementException:
                main_text = ""

            if main_text == None or main_text == "":
                try:
                    main_text = _driver.find_element_by_tag_name("h3")
                except selenium.common.exceptions.NoSuchElementException:
                    main_text = None

        if main_text == None:
            continue

        main_text = " ".join(search_description().get_elem_text(main_text).replace("\n", " ").replace("\r", " ").replace("\t", " ").split())

        remove_elems = _driver.find_elements_by_tag_name("script")
        for elem in remove_elems:
            _driver.execute_script("""
            var element = arguments[0];
            element.parentNode.removeChild(element);
            """, elem)
        remove_elems = _driver.find_elements_by_tag_name("style")
        for elem in remove_elems:
            _driver.execute_script("""
            var element = arguments[0];
            element.parentNode.removeChild(element);
            """, elem)
        remove_elems = _driver.find_elements_by_tag_name("noscript")
        for elem in remove_elems:
            _driver.execute_script("""
            var element = arguments[0];
            element.parentNode.removeChild(element);
            """, elem)


        body = _driver.find_element_by_tag_name("body")
        text = " ".join(search_description().remove_wierd(search_description().get_elem_text(body)).split())
        # limit description to 11k chars
        text = text[:11000]

        if main_text == "" or text == "":
            print(f"deep search: {extended_link_loc + link_loc} main_text or text were empty!")
            #print(main_text)
            #print(text)
            continue

        with open(f"deepcrawl/deepcrawl-{sleeptime}.bin", "a", encoding="utf8") as f:
            print(f"deep add: {extended_link_loc + link_loc}\t{main_text}")
            #print("deep added: {extended_link_loc + link_loc} data")
            f.write(f"{extended_link_loc + link_loc}\t{main_text}\t{text}\n")

def chech_link(_driver, _etc_driver, _got_link, sleeptime):
    while True:
        try:
            _driver.get(f"http://127.0.0.1:43110/{_got_link['id']}/")
            break
        except selenium.common.exceptions.InvalidArgumentException:
            _driver.switch_to_default_content()
            continue
        except selenium.common.exceptions.TimeoutException:
            print(f"{_got_link['id']} didn't load in time")
            break
        except Exception:
            return


    time.sleep(2)

    try:
        # update site crawl data
        res = get_crawl_line(_got_link['id'], _driver, _etc_driver)
    except selenium.common.exceptions.StaleElementReferenceException:
        return

    if res is None:
        # site might be loading
        #### rm that because were going to load sties we might got already
        #file_access(debug=False).move_to_loading(_got_link["id"])
        return


    # check if we have nupdated crawl line
    if not f"{_got_link['line']}".startswith(res) or not res.startswith(f"{_got_link['line']}"):
        file_access(debug=False).update_crawl_line(_got_link['id'], res, f"crawls/crawl-{sleeptime}.bin")
        print(f"updated {_got_link['id']}")


    #search_links(_driver, sleeptime)
    search_deep(_driver, sleeptime)


if __name__ == "__main__":
    options = Options()
    options.headless = True
    threads_limit = 12
    drivers = []
    etc_drivers = []

    # set drivers
    if platform.system() == "Windows":
        for i in range(0, threads_limit):
            driver = webdriver.Firefox(executable_path="./geckodriver")
            etc_driver = webdriver.Firefox(executable_path="./geckodriver", options=options)
            driver.set_page_load_timeout(5);
            etc_driver.set_page_load_timeout(5);

            drivers.append(driver)
            etc_drivers.append(etc_driver)
    else:
        for i in range(0, threads_limit):
            driver = webdriver.Firefox(executable_path="./geckodriver")
            etc_driver = webdriver.Firefox(executable_path="./geckodriver", options=options)
            driver.set_page_load_timeout(5);
            etc_driver.set_page_load_timeout(5);

            drivers.append(driver)
            etc_drivers.append(etc_driver)

    # read current scan
    current_crawl_status = file_access(debug=True).get_current()

    # read what to ignore
    # append this cause we want to have predefined vars also
    #ignore_sites.extend(file_access(debug=True).get_ignored())

    # extend loading sites right now, deal with them tom
    #ignore_sites.extend(file_access(debug=True).get_loading_sites(ignore_sites))

    # read crawl file
    #already_got_links.extend(file_access(debug=True).get_loading_sites(ignore_sites)[::-1])
    # read crawl files from crawls folder
    #already_got_links.extend(file_access(debug=True).get_ignored())
    crawl_files = [f for f in listdir("crawls") if isfile(join("crawls", f))]
    for file in crawl_files:
        already_got_links.extend(file_access(debug=True).get_crawl(ignore_sites, current_crawl_status, f"crawls/{file}"))

    found_current = True
    got_link_index = 0
    threads = []
    for i in range(0, threads_limit):
        threads.append(None)

    crawled_links = []
    while got_link_index < len(already_got_links):
        got_link = already_got_links[got_link_index]
        got_link_index += 1

        try:
            if got_link["id"][0] == '+':
                got_link["id"] = got_link["id"].split("|")[1]
        except Exception:
            continue

        if not found_current:
            if current_crawl_status == got_link["id"]:
                found_current = True
            else:
                continue

        if got_link["id"] in totally_ignore_url:
            continue

        if got_link["id"] in crawled_links:
            continue

        crawled_links.append(got_link["id"])

        file_access(debug=False).set_current_crawl_status(got_link["id"])
        print(f"----------: got_link_index {got_link_index}")

        got_thread = False
        while not got_thread:
            for i, thread in enumerate(threads):
                if thread == None:
                    t = threading.Thread(target=chech_link, args=(drivers[i], etc_drivers[i], got_link, i))
                    t.start()
                    threads[i] = t
                    got_thread = True
                    break
                if not thread.is_alive():
                    threads[i] = None

            time.sleep(0.1)

    time.sleep(10)

    stil_running = True
    while not stil_running:
        stil_running = False
        for thread in threads:
            if thread.is_alive():
                stil_running = True

    for driver in drivers:
        driver.quit();

    for etc_driver in etc_drivers:
        etc_driver.quit();
