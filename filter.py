import re
import html
import sqlite3
import time

from html.parser import HTMLParser

from os import listdir
from os.path import isfile, join


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return '\n'.join(self.fed)

def is_acceptable_line(line):
    if line == "":
        return None

    splitted = line.split("\t")

    if len(splitted) <= 1:
        return None

    if splitted[1] == "<load title>":
        return None

    s = MLStripper()
    s.feed(splitted[1])

    if  html.unescape(s.get_data()).replace(" ", "").strip() == "":
        return None

    return splitted;

if __name__ == "__main__":
    new_lines = ""
    got_ids = []

    crawl_files = [f for f in listdir("crawls") if isfile(join("crawls", f))]

    for file in crawl_files:
        with open(f"crawls/{file}", "r") as f:
            lines = f.read().split("\n")

            for line in lines:
                # clear line from chars we dont want
                line = re.sub(r"<[^>]*>", "", line)
                splitted = is_acceptable_line(line)

                if splitted is None:
                    continue

                if splitted[0][0] == '+':
                    urls = splitted[0][1:].split("|")
                    # lowercase .bit url
                    urls[1] = urls[1].lower()

                    if urls[0] not in got_ids and urls[1] not in got_ids:
                        new_lines += line + "\n"
                        got_ids.extend(urls)

    for file in crawl_files:
        with open(f"crawls/{file}", "r") as f:
            lines = f.read().split("\n")

            for line in lines:
                line = re.sub(r"<[^>]*>", "", line)
                splitted = is_acceptable_line(line)
                if splitted is None:
                    continue

                if splitted[0][0] == '+':
                    continue

                if splitted[0].lower().endswith(".bit"):
                    if splitted[0].lower() not in got_ids:
                        new_lines += line + "\n"
                        got_ids.append(splitted[0].lower())
                else:
                    if splitted[0] not in got_ids:
                        new_lines += line + "\n"
                        got_ids.append(splitted[0])


    """
    no packing right now

    lines = new_lines.split("\n")
    words = [x for y in [l.split(" ") for l in lines] for x in y]
    most_common_ones = sorted([(w, words.count(w)) for w in set(words)], key = lambda x:x[1], reverse=True)

    converts = []
    converted_words = 0
    indexo = 0
    while converted_words < 28:
        word = most_common_ones[indexo][0]
        word_count = most_common_ones[indexo][1]

        if len(word) >= 4:
            print(f"convert {word} {word_count}")

            for i in range(0, 128):
                c = chr(i)
                if c not in new_lines:
                    print(f"{i} {c} can be used")
                    new_lines = new_lines.replace(" "+word+" ", c)
                    converted_words += 1
                    converts.append(f"{i}={word}")
                    break


        indexo += 1
    new_lines = "\t".join(converts) + "\n" + new_lines
    """

    with open("crawled.txt", "w") as f:
        f.write(new_lines)

    """
    conn = sqlite3.connect("crawled-1.sqlite3")
    c = conn.cursor()

    for line in new_lines.split("\n"):
        splitted = is_acceptable_line(line)
        if splitted is None:
            continue

        url = splitted[0]
        title = splitted[1]
        description = ""

        for i in range(2, len(splitted)):
            description += splitted[i] + ("" if len(splitted) -1 else " ")


        c.execute('insert into links (url, title, description) VALUES (?, ?, ?)', [url, title, description])
    conn.commit()
    """
