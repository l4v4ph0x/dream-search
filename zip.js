var fs = require('fs');
var path = require('path');

var filePath = path.join(__dirname, 'deepcrawled.txt');

// LZW-compress a string
function lzw_encode(s) {
    var dict = {};
    var data = (s + "").split("");
    var out = [];
    var currChar;
    var phrase = data[0];
    var code = 256;
    for (var i=1; i<data.length; i++) {
        currChar=data[i];
        if (dict[phrase + currChar] != null) {
            phrase += currChar;
        }
        else {
            out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
            dict[phrase + currChar] = code;
            code++;
            phrase=currChar;
        }
    }
    out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
    for (var i=0; i<out.length; i++) {
        out[i] = String.fromCharCode(out[i]);
    }
    return out.join("");
}

// Decompress an LZW-encoded string
function lzw_decode(s) {
    var dict = {};
    var data = (s + "").split("");
    var currChar = data[0];
    var oldPhrase = currChar;
    var out = [currChar];
    var code = 256;
    var phrase;
    for (var i=1; i<data.length; i++) {
        var currCode = data[i].charCodeAt(0);
        if (currCode < 256) {
            phrase = data[i];
        }
        else {
           phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
        }
        out.push(phrase);
        currChar = phrase.charAt(0);
        dict[code] = oldPhrase + currChar;
        code++;
        oldPhrase = phrase;
    }
    return out.join("");
}

fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data) {
    if (!err) {
      var newLines = "";
      var lines = data.split("\n");

      lines.forEach(function(line) {
        var compressed = lzw_encode(line.substr(0, 10));
        if (lzw_decode(compressed) == line.substr(0, 10)) {
          newLines += compressed + "\n";
        } else {
          newLines += line + "\n";
        }
      });

      fs.writeFile("deepcrawled.bin", newLines, function(err) {
          if (err) {
              return console.log(err);
          }

          console.log("The file was saved!");
      });
    } else {
        console.log(err);
    }
});
