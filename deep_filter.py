import re
import html

from html.parser import HTMLParser

from os import listdir
from os.path import isfile, join


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return '\n'.join(self.fed)

def is_acceptable_line(line):
    if line == "":
        return None

    splitted = line.split("\t")

    if len(splitted) <= 1:
        return None

    if splitted[1] == "<load title>":
        return None

    s = MLStripper()
    s.feed(splitted[1])

    if  html.unescape(s.get_data()).replace(" ", "").strip() == "":
        return None

    return splitted;

if __name__ == "__main__":
    new_lines = []
    got_ids = []
    got_plus_ids = []
    bits = {}
    already_deep = []
    words = []

    crawl_files = [f for f in listdir("crawls") if isfile(join("crawls", f))]
    deep_crawl_files = [f for f in listdir("deepcrawl") if isfile(join("deepcrawl", f))]

    for file in crawl_files:
        with open(f"crawls/{file}", "r") as f:
            lines = f.read().split("\n")

            for line in lines:
                # clear line from chars we dont want
                line = re.sub(r"<[^>]*>", "", line)
                splitted = is_acceptable_line(line)

                if splitted is None:
                    continue

                if splitted[0][0] == '+':
                    urls = splitted[0][1:].split("|")
                    # lowercase .bit url
                    urls[1] = urls[1].lower()

                    if urls[0] not in got_ids and urls[1] not in got_ids:
                        got_ids.extend(urls)
                        bits[urls[1]] = urls[0]

    for file in crawl_files:
        with open(f"crawls/{file}", "r") as f:
            lines = f.read().split("\n")

            for line in lines:
                line = re.sub(r"<[^>]*>", "", line)
                splitted = is_acceptable_line(line)
                if splitted is None:
                    continue

                if splitted[0][0] == '+':
                    continue

                if splitted[0].lower().endswith(".bit"):
                    if splitted[0].lower() not in got_ids:
                        got_ids.append(splitted[0].lower())
                else:
                    if splitted[0] not in got_ids:
                        got_ids.append(splitted[0])

    for file in deep_crawl_files:
        with open(f"deepcrawl/{file}", "r") as f:
            lines = f.read().split("\n")

            for line in lines:
                line = re.sub(r"<[^>]*>", "", line)
                line = line[len("http//:127.0.0.1:43110/"):]

                splitted = is_acceptable_line(line)

                if splitted is None:
                    continue

                if splitted[2].startswith("Not Found http"):
                    continue

                if splitted[2].lower().endswith("index.html"):
                    continue

                if "https://" in splitted[0] or "www." in splitted[0]:
                    continue

                if any(x in splitted[0] for x in [".com", ".co.uk", ".net", ".org", ".io"]):
                    continue

                # we have prob with empty title somehow
                #if len(splitted[1]) <= 2:
                #    continue

                if "#" in splitted[0]:
                    continue

                if "/" not in splitted[0]:
                    continue

                url_split = splitted[0].split("/")

                if url_split[0].lower().endswith(".bit"):
                    bit_url = url_split[0].lower()
                    hash_url = "" if bit_url not in bits.keys() else bits[bit_url]

                    if url_split[1] not in [a["deep"] for a in already_deep if a["url"] in [bit_url, hash_url]]:
                        if splitted[1] not in [a["title"] for a in already_deep if a["url"] in [bit_url, hash_url]]:
                            words.extend(splitted[1].split(" "))
                            words.extend(splitted[2].split(" "))

                            if hash_url != "":
                                new_lines.append(f"-+{hash_url}|{bit_url}/{url_split[1]}\t{splitted[1]}\t{splitted[2]}\n")
                            else:
                                new_lines.append("-" + line + "\n")
                            already_deep.append({"url": bit_url, "deep": url_split[1], "title": splitted[1]})
                else:
                    hash_url = url_split[0]
                    bit_url_keys = [k for k in bits.keys() if bits[k] == hash_url]
                    bit_url = "" if len(bit_url_keys) == 0 else bits[bit_url_keys[0]]
                    # !!! logic error some where, but that fixed something and not 100
                    bit_url = bit_url if bit_url.endswith(".bit") else ""

                    if url_split[1] not in [a["deep"] for a in already_deep if a["url"] in [hash_url, bit_url]]:
                        if splitted[1] not in [a["title"] for a in already_deep if a["url"] in [bit_url, hash_url]]:
                            words.extend(splitted[1].split(" "))
                            words.extend(splitted[2].split(" "))

                            if bit_url != "":
                                new_lines.append(f"-+{hash_url}|{bit_url}/{url_split[1]}\t{splitted[1]}\t{splitted[2]}\n")
                            else:
                                new_lines.append("-" + line + "\n")
                            already_deep.append({"url": hash_url, "deep": url_split[1], "title": splitted[1]})


    ws = set(words)
    maxws = len(ws)

    print(f"words {maxws}")
    """
    countsed = []
    percent = 0

    for i, w in enumerate(ws):
        # thers no point to compress 4 or les bytes words
        if len(w) < 3:
            continue

        countsed.append([w, words.count(w)])

        p = i * 100 // maxws

        if p != percent:
            percent = p
            print(f"{p}%")

        # limit to test cause it take 30min to compress
        if i >= 1000:
            break


    most_common_ones = sorted(countsed, key=lambda x: x[1], reverse=True)

    print(most_common_ones[:10])
    print(len(most_common_ones))

    converts = []
    converted_words = 0
    indexo = 0
    lasti = 32

    while converted_words < len(most_common_ones):
        word = most_common_ones[indexo][0]
        word_count = most_common_ones[indexo][1]

        if word_count < 500:
            print("under word count")

            break

        print(f"convert {word} {word_count}")

        found = False
        for i in range(lasti, 2097152):
            lasti = i
            c = chr(i)

            #if i >= 157 and i <= 200:
            #    continue

            if c == '<' or c == '>' or c == '/':
                continue

            if c not in new_lines:
                print(f"{i} can be used")
                new_lines = new_lines.replace(word, c)
                converted_words += 1
                converts.append(f"{i}={word}")
                found = True
                break

        if not found:
            print("no more found")
            break


        indexo += 1
    new_lines = "\t".join(converts) + "\n" + new_lines
    """

    lines_per_file = (len(new_lines) // 10) + 1
    for i in range(0, 10):
        for j in range(i * lines_per_file, i * lines_per_file + lines_per_file):
            with open(f"deepcrawled-{i}.txt", "a") as f:
                f.write(new_lines[j] + "\n")
